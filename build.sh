sudo cp -r images /images
mkdir -p app
go get -u github.com/FiloSottile/gvt
gvt restore
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app/main ./cmd/cataloguesvc
